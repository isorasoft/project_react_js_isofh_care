import React, { Component } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import { Container } from 'reactstrap';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import {
    AppAside,
    AppBreadcrumb,
    AppHeader,
    AppSidebar,
    AppSidebarFooter,
    AppSidebarForm,
    AppSidebarHeader,
    AppSidebarMinimizer,
    AppSidebarNav,
} from '@coreui/react';
// routes config
import routes from './configs/routes';
import DefaultAside from './components/layout/DefaultAside';
import DefaultHeader from './components/layout/DefaultHeader';
import WithRoot from './WithRoot';
import './App.scss';
class Home extends Component {

    render() {
        const { classes } = this.props;
        return (
            <div className="app">
                <AppHeader fixed>
                    <DefaultHeader />
                </AppHeader>
                <div className="app-body">
                    <AppSidebar fixed display="lg">
                        <AppSidebarHeader />
                        <AppSidebarForm />
                        <AppSidebarNav className={classes.sidebar} navConfig={{
                            items: [
                                {
                                    name: "Dashboard",
                                    url: '/admin/dashboard',
                                    icon: 'icon-speedometer',
                                }]
                        }} {...this.props} />
                        {/* <AppSidebarNav className={classes.sidebar} children={
              <div>
                Lorem data is sum
              </div>
            } /> */}
                        <AppSidebarFooter />
                        <AppSidebarMinimizer />
                    </AppSidebar>
                    <main className="main">
                        <AppBreadcrumb appRoutes={routes} />
                        <Container fluid>
                            <Switch>
                                <Route path="/admin/dashboard" />
                            </Switch>
                            {/* {routes.map((route, idx) => {
                                return route.component ? (
                                    <Route key={idx}
                                        path={route.path}
                                        exact={route.exact}
                                        name={route.name}
                                        render={props => (
                                            <route.component {...props} />
                                        )} />)
                                    : (null);
                            },
                            )} */}
                            <Redirect from="/admin" to="/admin/dashboard" />
                        </Container>
                    </main>
                </div>
                {/* <AppFooter>
          <DefaultFooter />
        </AppFooter> */}
            </div>
        );
    }
}

Home.propTypes = {
    classes: PropTypes.object.isRequired,
};

const styles = theme => ({

})

function mapStateToProps(state) {
    return {
        currentUser: state.currentUser
    };
}
export default connect(mapStateToProps)(WithRoot(withStyles(styles)(Home)));