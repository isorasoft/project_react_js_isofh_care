
const defaultState = {
    userApp:
    {
        currentUser: {

        },
        isLogin: false,
        loginToken: ""
    }
}
const reducer = (state = defaultState, action) => {
    var newState = JSON.parse(JSON.stringify(state));
    switch (action.type) {
        case "increase":
            if (!newState.count)
                newState.count = 0;
            newState.count++;
            return newState;
        case "reduced":
            if (!newState.count || newState.count < 0)
                newState.count = 1;
            newState.count--;
            return newState;
    }
    return newState;
}

export default reducer;