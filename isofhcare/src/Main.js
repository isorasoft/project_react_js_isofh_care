import React, { Component } from 'react';
import './App.css';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { connect } from 'react-redux';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import Home from "./sites/user/containners/Home";
import Admin from "./sites/admin/Home";
import clientUtils from "./utils/client-utils";
import userProvider from "./data-access/user-provider";
import objectUtils from "./utils/object-utils";
// import Stuff from "./test/Stuff";

class Main extends Component {
  constructor(props) {
    super(props);
    this.state = {}
  }
  async componentWillMount() {
    objectUtils.clone({});
    var user = await userProvider.getAccountStorage();
    // if (user) {

    // }
  }
  componentDidMount() {
  }
  render() {
    return (
      <div className="App">
        <Router>
          <div>
            {/* <Link to="/">Home</Link>
            <Link to="/about">Admin</Link> */}
            <Route exact path="/" component={Home} />
            <Route exact path={["/admin", "/admin/:function"]} component={Admin} />
          </div>
        </Router>

        <ToastContainer />
      </div>
    );
  }
}
function mapStateToProps(state) {
  return {
  };
}
export default connect(mapStateToProps)(Main);