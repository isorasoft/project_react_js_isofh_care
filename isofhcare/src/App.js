import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import Main from './Main';
import { BrowserRouter } from 'react-router-dom'

import AppReducer from './reducers';

const store = createStore(AppReducer, applyMiddleware(thunk));

const Kernel = () => (
  <Provider store={store}>
    <BrowserRouter>
      <Main />
    </BrowserRouter>
  </Provider>
)
export default Kernel;