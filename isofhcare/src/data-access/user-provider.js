import client from '../utils/client-utils';
import stringUtils from 'mainam-react-native-string-utils';
import constants from '../resources/strings';
import datacacheProvider from './datacache-provider';

export default {
    getAccountStorage() {
        return datacacheProvider.read("", constants.key.storage.current_account);
    },
    saveAccountStorage(account) {
        return datacacheProvider.save("", constants.key.storage.current_account, account);
    },
    xx() {
        client.serverApi = "";
    }
}   