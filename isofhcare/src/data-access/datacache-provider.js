module.exports = {
    save(userId, key, value) {
        return new Promise((resolve, reject) => {
            try {
                var data = {
                    value
                };
                localStorage.setItem(userId + "_" + key, JSON.stringify(data));
                resolve(true);
            } catch (error) {
                reject(error);
            }
        })
    },
    read(userId, key, defaultValue) {
        return new Promise((resolve, reject) => {
            if (localStorage.hasOwnProperty(userId + "_" + key)) {
                var item = localStorage.getItem(userId + "_" + key);
                if (item)
                    try {
                        var data = JSON.parse(item);
                        if (data && data.value) {
                            resolve(data.value);
                            return;
                        }
                    } catch (error) {
                    }
            }
            resolve(defaultValue);
        });
    }
}